## Sicredi-Digital-QA-api-master

API automation challenge using RestAssured.

#### API documentation and rules

The API rules along with their implementation and execution instructions are here at this [link](https://github.com/rh-southsystem/Sicredi-Digital-QA). 

#### Requirements and technologies used in the project

For this project the following technologies were used:

- [Maven](https://maven.apache.org/)
- [Java](https://www.oracle.com/br/java/technologies/javase-downloads.html) 1.8 ou +

##### Run the project 

To run the project, navigate to the root folder with a terminal of your choice and use the command:

`$ mvn test`

The command will run all tests contained in the project.

To run specific test methods:

`$ mvn test -Dtest=Test1,Test2`

To run a specific group of tests:

`$ mvn test -DincludeGroups=TestGroup1,TestGroup2` 

To exclude a specific test set group, use the command:

`$ mvn test -DexcludeGroups=TestGroup3,TestGroup4`

To exclude a specific package containing a test suite, use the command:

`$ mvn test -Dtest="test.java.com.service.map.**"`

#### Status Report

Here at this [link](https://gitlab.com/hiagonfs/sicredi-digital-qa-api-master-mirror/-/blob/main/Status%20Report.pdf) you can check a project status report. 
