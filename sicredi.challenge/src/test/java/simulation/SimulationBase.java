package simulation;

import base.BaseAPI;
import factory.CpfDataFactory;
import factory.SimulationDataFactory;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public abstract class SimulationBase extends BaseAPI {

    public static final String BASE_PATH_SIMULATION = "simulacoes";
    protected final SimulationDataFactory simulationDataFactory;
    protected final CpfDataFactory cpfDataFactory;

    public SimulationBase() {
        simulationDataFactory = new SimulationDataFactory();
        cpfDataFactory = new CpfDataFactory();
    }

    protected Response getRequestAllSimulationsInDatabase() {
        Response response = when().
                get(BASE_PATH_SIMULATION).
                then().
                statusCode(HttpStatus.SC_OK).
                extract().response();
        return response;
    }

    protected Response getRequestSimulation(String cpf) {
        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpf)
                .when()
                .get(BASE_PATH_SIMULATION + "/{cpf}")
                .then()
                .extract().response();
        return response;
    }

    protected Response postRequest(SimulationPOJO simulation) {
        Response response = given()
                .contentType(ContentType.JSON)
                .and()
                .body(simulation)
                .when()
                .post(BASE_PATH_SIMULATION)
                .then()
                .extract().response();
        return response;
    }

    protected Response putRequest(String cpf, SimulationPOJO simulationPOJO) {
        Response response = given().
                contentType(ContentType.JSON).
                pathParam("cpf", cpf).
                body(simulationPOJO).
                when().
                put(BASE_PATH_SIMULATION + "/{cpf}").
                then().
                extract().response();
        return response;
    }

    protected Response deleteRequest(int id) {
        Response response = given().
                pathParam("id", id).
                when().
                delete(BASE_PATH_SIMULATION + "/{id}").
                then().
                extract().response();
        return response;
    }

}
