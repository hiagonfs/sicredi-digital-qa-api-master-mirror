package restrictions;

import base.BaseAPI;
import factory.CpfDataFactory;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public abstract class RestrictionBase extends BaseAPI {

    public static final String BASE_PATH_RESTRICTION = "restricoes/{cpf}";
    protected final CpfDataFactory cpfDataFactory;

    public RestrictionBase() {
        cpfDataFactory = new CpfDataFactory();
    }

    protected Response getRestrictionResponse(String cpf) {
        Response response = given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpf)
                .when()
                .get(BASE_PATH_RESTRICTION)
                .then()
                .extract().response();
        return response;
    }

}
